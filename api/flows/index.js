const categoryList = require('../data/categories/categoryList');

function flowDelete(req, res) {
  const parts = req.url.split('/');

  const success = deleteFlow(parts[2]);

  if (!success) {
    return res.json({
      message: 'Flow not found',
    }, 404);
  }

  return res.json('', 204);
}

module.exports = { flowDelete };

function deleteFlow(flowId) {
  let found = false;

  categoryList.forEach((category) => {
    category.flows = category.flows.filter((flow) => {
      const match = flow.id === flowId;

      found = found || match;

      return !match;
    });
  });

  return found;
}
