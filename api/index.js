const http = require('http');
const categories = require('./categories/index');
const flows = require('./flows/index');

const flowByIdRe = /^\/flows\/[^\/]+$/;

const requestListener = function (req, res) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, POST, GET, DELETE');
  res.setHeader('Access-Control-Max-Age', 2592000); // 30 days

  if (req.method === 'OPTIONS') {
    res.writeHead(204);
    return res.end();
  }

  if (req.url === '/categories') {
    return categories.categoriesIndex(req, res);
  }

  if (flowByIdRe.test(req.url) && req.method === 'DELETE') {
    return flows.flowDelete(req, res);
  }

  res.json({
    message: 'Not found',
  }, 404);
}

const requestHandler = function (req, res) {
  res.json = resJson;

  try {
    requestListener(req, res);
  } catch (err) {
    res.json({
      message: err.toString(),
    }, 500);
  }
}

const server = http.createServer(requestHandler);
server.listen(8080);

function resJson(data, code = 200) {
  const jsonStr = JSON.stringify(data);

  this.writeHead(code, {
    'Content-Type': 'application/json',
  });
  this.end(jsonStr);
}