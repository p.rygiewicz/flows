const categoryList = require('../data/categories/categoryList');

function categoriesIndex(req, res) {
  res.json({
    categories: categoryList,
  });
}

module.exports = { categoriesIndex };
