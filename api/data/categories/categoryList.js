const dataJson = require('./data.json');

const categoryList = adaptCategoryList(dataJson);

module.exports = categoryList;

function adaptCategoryList(data) {
  data = data || {};

  if (!Array.isArray(data.categories)) {
    return [];
  }

  return data.categories.map((category) => {
    category = category || {};

    const title = String(category.title)

    return {
      name: title,
      id: title.toLowerCase().split(' ').join('-'),
      flows: adaptCategoryFlowList(category),
    }
  });
}

function adaptCategoryFlowList(data) {
  data = data || {};

  if (!Array.isArray(data.flows)) {
    return [];
  }

  return data.flows.map((flow) => {
    flow = flow || {};

    const code = String(flow.code);

    return {
      name: String(flow.name || code),
      id: code.toLowerCase(),
      code,
    }
  });
}
