export const CategoriesService = {
  getAll() {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, 1000); // pretend to take longer
    })
      .then(() => {
        return fetch('http://localhost:8080/categories')
      })
      .then((response) => {
        if (firstDigit(response.status) === 2) {
          return response.json();
        }

        return {
          error: 'Error: ' + response.status,
        };
      })
      .catch((error) => {
        return {
          error: error.toString(),
        };
      })
  },

  deleteFlow(flowId) {
    return fetch('http://localhost:8080/flows/' + flowId, { method: 'DELETE' })
      .then((response) => {
        if (firstDigit(response.status) === 2) {
          return {};
        }

        return {
          error: 'Error: ' + response.status,
        };
      })
      .catch((error) => {
        return {
          error: error.toString(),
        };
      })
  }
}

function firstDigit(num) {
  return Math.floor(num / 100);
}
