import React from 'react';
import './App.css';
import { NotFound } from './scenes/NotFound';
import { Router, Redirect } from '@reach/router';
import { Operations } from './scenes/Operations';
import { CssBaseline, Container } from '@material-ui/core';


function App() {
  return (
    <Container>
      <CssBaseline />
      <Router>
        <Redirect from="/" to="/categories" noThrow />
        <Operations path="categories/*"></Operations>
        <NotFound default></NotFound>
      </Router>
    </Container>
  );
}

export default App;
