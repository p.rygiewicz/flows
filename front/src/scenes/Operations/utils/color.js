const accentColors = [
  '#DAF7A6', '#FFC300', '#FF5733', '#C70039', '#900C3F', '#581845', '#D35400', '#7F8C8D',
  '#5D6D7E', '#2C3E50', '#229954', '#CB4335',
];

function getAccentColor(index) {
  const accentIndex = index % accentColors.length;

  return accentColors[accentIndex];
}

export { getAccentColor };
