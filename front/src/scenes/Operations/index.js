import React, { useEffect, useState } from 'react';
import { Router } from '@reach/router';
import { CategoriesService } from '../../services/categories';
import { CircularProgress, Button, Snackbar } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { Categories } from './components/Categories';
import { Flows } from './components/Flows';
import { NotFound } from '../NotFound';
import './index.css';

function Operations() {
  const [pendingState, setPendingState] = useState(false);

  const [errorState, setErrorState] = useState(null);

  const [categoriesState, setCategoriesState] = useState({
    categories: [],
    error: null,
  });

  function getCategories() {
    return apiGetCategories(setCategoriesState, setPendingState);
  }

  function deleteFlow(flowId) {
    apiDeleteFlow(flowId, setCategoriesState, setPendingState, setErrorState);
  }

  useEffect(() => {
    getCategories();
  }, []);

  function onTryAgain() {
    getCategories();
  }

  function onFlowDelete(flowId) {
    deleteFlow(flowId);
  }

  function onErrorClose() {
    setErrorState(null);
  }

  return (
    <div className="Operations-container">
      <Router>
        <Categories path="/" categories={categoriesState.categories}></Categories>
        <Flows path=":categoryId" categories={categoriesState.categories} onDelete={onFlowDelete}></Flows>
        <NotFound default></NotFound>
      </Router>
      {
        pendingState &&
        <div className="pending">
          <CircularProgress />
        </div>
      }
      {
        categoriesState.error &&
        <Alert
          severity="error"
          action={
            <Button color="inherit" size="small" onClick={onTryAgain}>
              Try again
          </Button>
          }
        >
          {categoriesState.error}
        </Alert>
      }
      <Snackbar
        open={!!errorState}
        autoHideDuration={6000}
        onClose={onErrorClose}
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      >
        <Alert severity="error">{errorState}</Alert>
      </Snackbar>
    </div>
  );
}

export { Operations };

async function apiGetCategories(setCategoriesState, setPendingState) {
  setPendingState(true);
  setCategoriesState({
    categories: [],
    error: null,
  });

  const result = await CategoriesService.getAll();

  setCategoriesState({
    categories: result.categories || [],
    error: result.error || null,
  });
  setPendingState(false);
}

async function apiDeleteFlow(flowId, setCategoriesState, setPendingState, setErrorState) {
  setPendingState(true);
  setErrorState(null);

  const result = await CategoriesService.deleteFlow(flowId);

  setPendingState(false);

  if (result.error) {
    setErrorState(result.error || null);
  } else {
    apiGetCategories(setCategoriesState, setPendingState);
  }
}
