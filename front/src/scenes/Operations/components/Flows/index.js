import React from 'react';
import './index.css';
import { FlowCard } from '../FlowCard';
import { Link } from '@reach/router';


function Flows(props) {
  const flowCategory = props.categories && props.categories.find((category) => {
    return category.id === props.categoryId;
  });

  const flows = (flowCategory && flowCategory.flows) || [];

  return (
    <div className="Flows-container">
      <Link to="../">back to categories</Link>
      <h1>Flows</h1>
      {
        flows.length ?
          <div className="flows">
            {
              flows.map((flow, index) => {
                return (
                  <div className="card" key={flow.id}>
                    <FlowCard
                      id={flow.id}
                      title={flow.name}
                      accent={index}
                      onDelete={props.onDelete}
                    ></FlowCard>
                  </div>
                );
              })
            }
          </div> : null
      }
    </div>
  );
}

export { Flows };
