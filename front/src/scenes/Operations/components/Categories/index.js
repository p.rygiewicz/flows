import React from 'react';
import './index.css';
import { CategoryCard } from '../CategoryCard';


function Categories(props) {
  function onCategoryClick(categoryId) {
    props.navigate && props.navigate(categoryId);
  }

  return (
    <div className="Categories-container">
      <h1>Categories</h1>
      {
        props.categories && props.categories.length ?
          <div className="categories">
            {
              props.categories.map((category, index) => {
                return (
                  <div className="card" key={category.id}>
                    <CategoryCard
                      id={category.id}
                      title={category.name}
                      accent={index}
                      onClick={onCategoryClick}
                    ></CategoryCard>
                  </div>
                );
              })
            }
          </div> : null
      }
    </div>
  );
}

export { Categories };
