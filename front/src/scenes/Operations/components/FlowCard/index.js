import React from 'react';
import { Card, CardContent, Typography, CardActions, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { getAccentColor } from '../../utils/color';


const useStyles = makeStyles({
  card: {
    display: 'flex',
    flexDirection: 'column',
  },
  accent: {
    display: 'block',
    height: 8,
  },
  actions: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
  }
});

function FlowCard(props) {
  const classes = useStyles();

  function onAction(name) {
    return () => {
      alert('Action: ' + name + '\r\nFlow: ' + props.title);
    }
  }

  function onDeleteClick() {
    const confirmed = window.confirm('Are you sure?');

    if(confirmed) {
      props.onDelete && props.onDelete(props.id);
    }
  }

  return (
    <Card className={classes.card}>
      <span className={classes.accent} style={{ backgroundColor: getAccentColor(props.accent) }}></span>
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
          {props.title}
        </Typography>
      </CardContent>
      <div className={classes.actions}>
        <CardActions>
          <Button size="small" color="primary" variant="outlined" disableElevation onClick={onAction('SCE')}>
            SCE
        </Button>
          <Button size="small" color="primary" variant="outlined" disableElevation onClick={onAction('API')}>
            API
        </Button>
          <Button size="small" color="primary" variant="outlined" disableElevation onClick={onAction('Postman')}>
            Postman
        </Button>
        </CardActions>
        <CardActions>
          <Button size="small" color="primary" variant="outlined" disableElevation onClick={onAction('Copy')}>
            Copy
        </Button>
        <Button size="small" color="secondary" variant="outlined" disableElevation onClick={onDeleteClick}>
            Delete
        </Button>
        </CardActions>
      </div>
    </Card>
  )
}

export { FlowCard };
