import React from 'react';
import { Card, CardActionArea, CardContent, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { getAccentColor } from '../../utils/color';


const useStyles = makeStyles({
  accent: {
    display: 'block',
    height: 8,
  }
});

function CategoryCard(props) {
  const classes = useStyles();

  function onClick() {
    props.onClick && props.onClick(props.id);
  }

  return (
    <Card>
      <span className={classes.accent} style={{ backgroundColor: getAccentColor(props.accent) }}></span>
      <CardActionArea onClick={onClick}>
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {props.title}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  )
}

export { CategoryCard };
