import React from 'react';
import logo from './logo.svg';
import './index.css';
import {Link} from '@reach/router';


function NotFound() {
  return (
    <div className="NotFound">
      <header className="NotFound-header">
        <img src={logo} className="NotFound-logo" alt="logo" />
        <p>
          Page not found
        </p>
        <Link to="/" className="NotFound-link">Visit homepage</Link>
      </header>
    </div>
  );
}

export { NotFound };
